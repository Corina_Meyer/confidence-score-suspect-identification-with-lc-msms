# Confidence Score Suspect Identification with LC-MSMS


This R code enables the calculation of the confidence score for suspects identified by LC-HRMS/MS in DDA mode. The calculation is based on Alygizakis et al., TrAC 159 (2023) 116944. The isotope fit score, the retention time score and the fragment score are calcuated individually and are summed to the final score.

As input, a csv file of the suspects to be scored is required. This csv file has to contain the compound name, the confidence level (according to Schymanski et al.), the monoisotopic mass, the measured retention time, the adduct, the ionization mode and the logDOW value.
Moreoever, the MS1 spectra (for the isotopic fit score) and the MS2 spectra (for the fragment score) need to be provided as txt files containing the m/z-intensity matrix.
The retention time information is incorporated via a linear regression between logDOW values and measured retention times of target compounds analyzed by the same chromatographic method. A csv file of these values is required as an additional input.
The provided example input covers a level 2a and a level 3 identification.

The output lists the individual scores and the final score for each suspect in separate columns.





